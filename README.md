# Opal

Opal is a note-taking and project management app. It is built using the MEAN stack including [Socket.io](https://socket.io/) to enhance real-time bidirectional communication performance with multiple clients.

## Installation

Use the package manager [npm](https://pip.pypa.io/en/stable/) to install.

```bash
git clone https://gitlab.com/kazmonroy/opal-frontend.git
npm install
ng serve --open
```

## Tools

[MondoDB](https://www.mongodb.com/) \
[Express](https://expressjs.com/) \
[Angular](https://angular.io/) \
[Node.js](https://nodejs.org) \
[Socket.io](https://socket.io/)

## Live app

https://opal-app.vercel.app/

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
