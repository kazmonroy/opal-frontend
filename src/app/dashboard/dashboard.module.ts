import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { InlineFormComponent } from '../shared/modules/inline-form/components/inline-form/inline-form.component';
import { TopBarComponent } from '../shared/modules/top-bar/components/top-bar.component';
import { SidebarComponent } from '../shared/modules/sidebar/components/sidebar.component';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    InlineFormComponent,
    TopBarComponent,
    SidebarComponent,
  ],
})
export class DashboardModule {}
