import { Component, inject } from '@angular/core';
import { ToggleService } from 'src/app/shared/services/toggle.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  private toggleService = inject(ToggleService);
  isSideBarOpen = this.toggleService.handleToggle();
}
