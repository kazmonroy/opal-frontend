import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Observable, combineLatest, map } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { BoardService } from 'src/app/board/services/board.service';
import { BoardsComponent } from 'src/app/boards/boards.component';
import { InlineFormComponent } from 'src/app/shared/modules/inline-form/components/inline-form/inline-form.component';
import { BoardsService } from 'src/app/shared/services/boards.service';
import { BoardInterface } from 'src/app/shared/types/board.interface';
import { TaskInterface } from 'src/app/shared/types/task.interface';

@Component({
  standalone: true,
  imports: [CommonModule, RouterModule, InlineFormComponent, BoardsComponent],
  selector: 'app-home-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.scss'],
})
export class HomeDashboardComponent implements OnInit {
  private authService = inject(AuthService);
  private boardService = inject(BoardService);
  private boardsService = inject(BoardsService);

  currentUser$ = this.authService.currentUser$;
  sidebarState!: boolean;
  boards: BoardInterface[] = [];
  data$!: Observable<{ boards: BoardInterface[]; tasks: TaskInterface[] }>;

  createBoard(title: string) {
    this.boardsService.createBoard(title).subscribe((newBoard) => {
      this.boards = [...this.boards, newBoard];
    });
  }

  ngOnInit(): void {
    this.data$ = combineLatest([
      this.boardsService.getBoards(),
      this.boardService.tasks$,
    ]).pipe(map(([boards, tasks]) => ({ boards, tasks })));

    this.data$.subscribe((res) => {
      if (res.boards.length === 0) {
        // console.log('empty');
      } else {
        // console.log('ok');
      }
    });
  }
}
