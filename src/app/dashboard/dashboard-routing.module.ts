import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/services/authGuard.guard';
import { BoardComponent } from '../board/component/board/board.component';
import { TaskModalComponent } from '../board/component/task-modal/task-modal.component';
import { HomeDashboardComponent } from './components/home-dashboard/home-dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BoardsComponent } from '../boards/boards.component';
import { ResultsComponent } from '../searchResults/results.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    title: 'Home',
    canMatch: [AuthGuard],
    children: [
      {
        path: 'home',
        component: HomeDashboardComponent,
        title: 'Home - Opal',
        canMatch: [AuthGuard],
      },
      {
        path: 'boards',
        component: BoardsComponent,
        title: 'Boards',
        canMatch: [AuthGuard],
      },
      {
        path: 'boards/:boardId',
        title: 'Boards',
        component: BoardComponent,
        canMatch: [AuthGuard],
        children: [
          {
            path: 'tasks/:taskId',
            title: 'Tasks',
            component: TaskModalComponent,
          },
        ],
      },
      {
        path: 'results',
        title: 'Search results',
        component: ResultsComponent,
        canMatch: [AuthGuard],
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
