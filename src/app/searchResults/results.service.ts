import { Injectable, inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TaskInterface } from '../shared/types/task.interface';
import { HttpClient } from '@angular/common/http';
import { UserInterface } from '../shared/types/users.interface';

@Injectable({
  providedIn: 'root',
})
export class ResultsService {
  private http = inject(HttpClient);

  private users$ = new BehaviorSubject<UserInterface[]>([]);
  users = this.users$.asObservable();

  private results$ = new BehaviorSubject<UserInterface[]>([]);
  allResults = this.results$.asObservable();

  getUsers() {
    this.http
      .get<UserInterface[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe((results) => {
        this.users$.next(results);
      });
  }

  updateResults(results: UserInterface[]) {
    this.results$.next(results);
  }
}
