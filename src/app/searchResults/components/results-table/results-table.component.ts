import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskInterface } from 'src/app/shared/types/task.interface';

@Component({
  selector: 'app-results-table',
  templateUrl: './results-table.component.html',
  styleUrls: ['./results-table.component.scss'],
  standalone: true,
  imports: [CommonModule],
})
export class ResultsTableComponent {
  @Input() data!: any;
  @Input() headers!: {
    key: string;
    subKey?: string | undefined;
    label: string;
  }[];
}
