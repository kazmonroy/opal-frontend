import { Component, OnInit, inject } from '@angular/core';
import { TaskInterface } from '../shared/types/task.interface';
import { ResultsService } from './results.service';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';
import { ResultsTableComponent } from './components/results-table/results-table.component';
import { UserInterface } from '../shared/types/users.interface';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  standalone: true,
  imports: [CommonModule, ResultsTableComponent],
})
export class ResultsComponent implements OnInit {
  private resultsService = inject(ResultsService);
  data$: Observable<UserInterface[]> = this.resultsService.allResults;

  headers = [
    {
      key: 'name',
      label: 'Name',
    },
    {
      key: 'username',
      label: 'Username',
    },
    {
      key: 'email',
      label: 'Email',
    },
    {
      key: 'phone',
      label: 'Phone',
    },
  ];

  ngOnInit(): void {}
}
