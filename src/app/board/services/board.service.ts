import { Injectable, inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SocketService } from 'src/app/shared/services/socket.service';
import { BoardInterface } from 'src/app/shared/types/board.interface';
import { ColumnInterface } from 'src/app/shared/types/column.interface';
import { SocketEventsEnum } from 'src/app/shared/types/socketEvents.enum';
import { TaskInterface } from 'src/app/shared/types/task.interface';

@Injectable({
  providedIn: 'root',
})
export class BoardService {
  private socketService = inject(SocketService);
  board$ = new BehaviorSubject<BoardInterface | null>(null);
  columns$ = new BehaviorSubject<ColumnInterface[]>([]);
  tasks$ = new BehaviorSubject<TaskInterface[]>([]);

  setBoard(board: BoardInterface) {
    this.board$.next(board);
  }

  setColumns(columns: ColumnInterface[]) {
    this.columns$.next(columns);
  }

  leaveBoard(boardId: string) {
    this.board$.next(null);
    this.socketService.emit(SocketEventsEnum.boardsLeave, { boardId });
  }

  addColumn(column: ColumnInterface) {
    // real-time update columns in FE
    const updatedColumns = [...this.columns$.getValue(), column];
    this.columns$.next(updatedColumns);
  }

  setTasks(tasks: TaskInterface[]) {
    this.tasks$.next(tasks);
  }

  addTask(task: TaskInterface) {
    const updatedTasks = [...this.tasks$.getValue(), task];
    this.tasks$.next(updatedTasks);
  }

  updateBoard(updatedBoard: BoardInterface) {
    const board = this.board$.getValue();
    const { title } = updatedBoard;

    if (!board) {
      throw new Error('Board not initialized');
    }

    this.board$.next({ ...board, title });
  }

  deleteColumn(columnId: string) {
    const updatedColumns = this.columns$
      .getValue()
      .filter((column) => column.id !== columnId);

    this.columns$.next(updatedColumns);
  }

  deleteTask(taskId: string) {
    const updatedTasks = this.tasks$
      .getValue()
      .filter((task) => task.id !== taskId);

    this.tasks$.next(updatedTasks);
  }

  updateColumn(columnUpdate: ColumnInterface) {
    const columns = this.columns$.getValue();
    const { title, id } = columnUpdate;

    if (!columnUpdate) {
      throw new Error('Column not established');
    }

    const updatedColumns = columns.map((column) => {
      return column.id === id ? { ...column, title } : column;
    });

    this.columns$.next(updatedColumns);
  }

  updateTask(updateTask: TaskInterface) {
    const { title, description, columnId } = updateTask;
    const updatedTasks = this.tasks$.getValue().map((task) => {
      return task.id === updateTask.id
        ? { ...task, title, description, columnId }
        : task;
    });

    this.tasks$.next(updatedTasks);
  }
}
