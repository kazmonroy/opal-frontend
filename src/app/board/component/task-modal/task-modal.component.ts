import { CommonModule, Location } from '@angular/common';
import {
  Component,
  HostBinding,
  HostListener,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BoardService } from '../../services/board.service';
import { TaskInterface } from 'src/app/shared/types/task.interface';
import {
  Observable,
  Subject,
  combineLatest,
  filter,
  map,
  takeUntil,
} from 'rxjs';
import { ColumnInterface } from 'src/app/shared/types/column.interface';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { TasksService } from 'src/app/shared/services/tasks.service';
import { SocketService } from 'src/app/shared/services/socket.service';
import { SocketEventsEnum } from 'src/app/shared/types/socketEvents.enum';
import { InlineFormComponent } from 'src/app/shared/modules/inline-form/components/inline-form/inline-form.component';
import { IconsModule } from 'src/app/shared/modules/icons/icons.module';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    InlineFormComponent,
    IconsModule,
    ReactiveFormsModule,
  ],
  selector: 'app-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.scss'],
})
export class TaskModalComponent implements OnInit, OnDestroy {
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private location = inject(Location);
  private boardService = inject(BoardService);
  private socketService = inject(SocketService);
  private tasksService = inject(TasksService);
  private fb = inject(FormBuilder);

  boardId!: string;
  taskId!: string;
  task$!: Observable<TaskInterface>;
  data$!: Observable<{ task: TaskInterface; columns: ColumnInterface[] }>;

  unsubscribe$ = new Subject<void>();

  columnForm = this.fb.group({
    columnId: [''],
  });

  @HostBinding('class') classes = 'task-modal';
  @HostListener('document:keydown', ['$event']) onKeydownHandler(
    event: KeyboardEvent
  ) {
    if (event.key === 'Escape') {
      this.goBackToBoard();
    }
  }

  ngOnInit(): void {
    this.boardId = this.route.parent?.snapshot.paramMap.get('boardId')!;
    this.taskId = this.route.snapshot.paramMap.get('taskId')!;

    if (!this.boardId) {
      throw new Error(`Can't get boardId from URL`);
    } else if (!this.taskId) {
      throw new Error(`Can't get taskId from URL`);
    }

    this.task$ = this.boardService.tasks$.pipe(
      map((tasks: TaskInterface[]) => {
        return tasks.find((task) => task.id === this.taskId);
      }),
      filter(Boolean)
    );

    this.data$ = combineLatest([this.task$, this.boardService.columns$]).pipe(
      map(([task, columns]) => ({
        task,
        columns,
      }))
    );

    this.task$.pipe(takeUntil(this.unsubscribe$)).subscribe((task) => {
      this.columnForm.patchValue({ columnId: task.columnId });
    });

    combineLatest([this.task$, this.columnForm.get('columnId')!.valueChanges])
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([task, columnId]) => {
        if (task.columnId !== columnId) {
          this.tasksService.updateTask(this.boardId, task.id, { columnId });
        }
      });

    this.socketService
      .listen<string>(SocketEventsEnum.tasksDeleteSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((taskId) => {
        this.goBackToBoard();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  goBackToBoard() {
    // this.router.navigate(['boards', this.boardId]);
    this.location.back();
  }

  updateTaskName(taskName: string) {
    this.tasksService.updateTask(this.boardId, this.taskId, {
      title: taskName,
    });
  }

  updateTaskDescription(taskDescription: string) {
    this.tasksService.updateTask(this.boardId, this.taskId, {
      description: taskDescription,
    });
  }

  deleteTask() {
    this.tasksService.deleteTask(this.boardId, this.taskId);
  }
}
