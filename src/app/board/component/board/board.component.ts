import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import {
  ActivatedRoute,
  NavigationStart,
  Router,
  RouterModule,
} from '@angular/router';
import { BoardsService } from 'src/app/shared/services/boards.service';
import { SocketService } from 'src/app/shared/services/socket.service';
import { BoardInterface } from 'src/app/shared/types/board.interface';
import { SocketEventsEnum } from 'src/app/shared/types/socketEvents.enum';
import { BoardService } from '../../services/board.service';
import { ColumnsService } from 'src/app/shared/services/columns.service';
import {
  Observable,
  Subject,
  combineLatest,
  filter,
  map,
  takeUntil,
} from 'rxjs';
import { ColumnInterface } from 'src/app/shared/types/column.interface';
import { ColumnInputRequest } from 'src/app/shared/types/columnRequest.interface';
import { TasksService } from 'src/app/shared/services/tasks.service';
import { TaskInterface } from 'src/app/shared/types/task.interface';
import { TaskInputRequest } from 'src/app/shared/types/TaskInputRequest.interface';
import { CommonModule } from '@angular/common';
import { InlineFormComponent } from 'src/app/shared/modules/inline-form/components/inline-form/inline-form.component';
import { IconsModule } from 'src/app/shared/modules/icons/icons.module';

@Component({
  standalone: true,
  imports: [CommonModule, RouterModule, InlineFormComponent, IconsModule],
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit, OnDestroy {
  private boardsService = inject(BoardsService);
  private boardService = inject(BoardService);
  private socketService = inject(SocketService);
  private columnsService = inject(ColumnsService);
  private tasksService = inject(TasksService);
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private unsubscribe$ = new Subject<void>();

  boardId!: string;
  data$!: Observable<{
    board: BoardInterface;
    columns: ColumnInterface[];
    tasks: TaskInterface[];
  }>;

  ngOnInit(): void {
    const currentBoardId = this.route.snapshot.paramMap.get('boardId');

    if (!currentBoardId) {
      throw new Error(`Can't get boardId from URL`);
    }

    this.boardId = currentBoardId;
    this.fetchBoardData();
    this.initializeListeners();

    this.socketService.emit(SocketEventsEnum.boardsJoin, {
      boardId: this.boardId,
    });

    this.data$ = combineLatest([
      this.boardService.board$.pipe(filter(Boolean)),
      this.boardService.columns$.pipe(filter(Boolean)),
      this.boardService.tasks$.pipe(filter(Boolean)),
    ]).pipe(
      map(([board, columns, tasks]) => ({
        board,
        columns,
        tasks,
      }))
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initializeListeners() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart && !event.url.includes('/boards/')) {
        this.boardService.leaveBoard(this.boardId);
      }
    });

    this.socketService
      .listen<ColumnInterface>(SocketEventsEnum.columnsCreateSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((column) => {
        this.boardService.addColumn(column);
      });

    this.socketService
      .listen<TaskInterface>(SocketEventsEnum.tasksCreateSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((task) => {
        this.boardService.addTask(task);
      });

    this.socketService
      .listen<BoardInterface>(SocketEventsEnum.boardsUpdateSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((updatedBoard) => {
        this.boardService.updateBoard(updatedBoard);
      });

    this.socketService
      .listen<void>(SocketEventsEnum.boardsDeleteSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.router.navigateByUrl('/user/boards');
      });

    this.socketService
      .listen<string>(SocketEventsEnum.columnsDeleteSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((columnId) => {
        this.boardService.deleteColumn(columnId);
      });

    this.socketService
      .listen<ColumnInterface>(SocketEventsEnum.columnsUpdateSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((updatedColumn) => {
        this.boardService.updateColumn(updatedColumn);
      });

    this.socketService
      .listen<TaskInterface>(SocketEventsEnum.tasksUpdateSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((updatedTask) => {
        this.boardService.updateTask(updatedTask);
      });

    this.socketService
      .listen<string>(SocketEventsEnum.tasksDeleteSuccess)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((taskId) => {
        this.boardService.deleteTask(taskId);
      });
  }

  fetchBoardData() {
    this.boardsService.getBoard(this.boardId).subscribe((board) => {
      this.boardService.setBoard(board);
    });

    this.columnsService.getColumns(this.boardId).subscribe((columns) => {
      this.boardService.setColumns(columns);
    });

    this.tasksService.getTasks(this.boardId).subscribe((tasks) => {
      this.boardService.setTasks(tasks);
    });
  }

  createColumn(title: string) {
    const columnInput: ColumnInputRequest = {
      title,
      boardId: this.boardId,
    };

    this.columnsService.createColumn(columnInput);
  }

  getTasksByColumn(columnId: string, tasks: TaskInterface[]): TaskInterface[] {
    return tasks.filter((task) => task.columnId === columnId);
  }

  createTask(title: string, columnId: string) {
    const taskInput: TaskInputRequest = {
      title,
      boardId: this.boardId,
      columnId,
    };

    this.tasksService.createTask(taskInput);
  }

  updateBoardName(boardName: string) {
    const fields = {
      title: boardName,
    };

    this.boardsService.updateBoard(this.boardId, fields);
  }

  deleteBoard() {
    if (confirm('Are you sure you want to delete the board?')) {
      this.boardsService.deleteBoard(this.boardId);
    }
  }

  deleteColumn(columnId: string) {
    if (confirm('Are you sure you want to delete this column?')) {
      this.columnsService.deleteColumn(this.boardId, columnId);
    }
  }

  updateColumn(title: string, columnId: string) {
    const fields = {
      title,
    };
    this.columnsService.updateColumn(this.boardId, columnId, fields);
  }

  openTask(taskId: string) {
    this.router.navigate(['user', 'boards', this.boardId, 'tasks', taskId]);
  }
}
