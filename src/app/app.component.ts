import { Component, OnInit, inject } from '@angular/core';
import { AuthService } from './auth/services/auth.service';
import { SocketService } from './shared/services/socket.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  private authService = inject(AuthService);
  private socketService = inject(SocketService);
  currentUser = this.authService.currentUser$;

  ngOnInit(): void {
    this.authService.getCurrentUser().subscribe({
      next: (currentUser) => {
        this.authService.setCurrentUser(currentUser);
        this.socketService.setupSocketConnection(currentUser);
        this.currentUser = this.authService.currentUser$;
      },
      error: (err) => {
        this.authService.setCurrentUser(null);
      },
    });
  }
}
