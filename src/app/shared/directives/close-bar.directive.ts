import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  inject,
} from '@angular/core';

@Directive({
  selector: '[appCloseBar]',
  standalone: true,
})
export class CloseBarDirective {
  private elementRef = inject(ElementRef);

  @HostListener('document:click', ['$event'])
  onClickOut(event: any) {
    if ((event.target as HTMLElement).tagName !== 'NAV') {
      this.elementRef.nativeElement.classList.add('toggle');
    }
  }
}
