import { Directive, ElementRef, HostListener, inject } from '@angular/core';

@Directive({
  selector: '[appToggleDropdown]',
  standalone: true,
})
export class ToggleDropdownDirective {
  private elementRef = inject(ElementRef);

  @HostListener('document:click', ['$event'])
  onClick(event: any) {
    if ((event.target as HTMLElement).classList.contains('user-profile')) {
      this.elementRef.nativeElement.classList.toggle('toggle');
    } else if ((event.target as HTMLElement).tagName !== 'NAV') {
      this.elementRef.nativeElement.classList.remove('toggle');
    }
  }
}
