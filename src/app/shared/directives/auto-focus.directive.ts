import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostListener,
  inject,
} from '@angular/core';

@Directive({
  selector: '[appAutofocus]',
  standalone: true,
})
export class AutoFocusDirective {
  private elementRef = inject(ElementRef);

  @HostListener('document:keydown', ['$event']) onKeydownHandler(
    event: KeyboardEvent
  ) {
    if (event.key === '/') {
      this.elementRef.nativeElement.focus();
    } else if (event.key === 'Escape') {
      this.elementRef.nativeElement.blur();
    }
  }
}
