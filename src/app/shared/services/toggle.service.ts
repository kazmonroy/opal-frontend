import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ToggleService {
  private sidebarBarState$ = new BehaviorSubject<boolean>(true);

  constructor() {}

  handleToggle() {
    const updatedState = this.sidebarBarState$.getValue();
    this.sidebarBarState$.next(!updatedState);
    return this.sidebarBarState$;
  }
}
