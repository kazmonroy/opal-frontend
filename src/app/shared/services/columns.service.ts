import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ColumnInterface } from '../types/column.interface';
import { environment } from 'src/environments/environment';
import { ColumnInputRequest } from '../types/columnRequest.interface';
import { SocketService } from './socket.service';
import { SocketEventsEnum } from '../types/socketEvents.enum';

@Injectable({
  providedIn: 'root',
})
export class ColumnsService {
  private http = inject(HttpClient);
  private socketService = inject(SocketService);
  private url = environment.apiUrl + '/boards';

  getColumns(boardId: string): Observable<ColumnInterface[]> {
    return this.http.get<ColumnInterface[]>(`${this.url}/${boardId}/columns`);
  }

  createColumn(columnInput: ColumnInputRequest) {
    this.socketService.emit(SocketEventsEnum.columnsCreate, columnInput);
  }

  deleteColumn(boardId: string, columnId: string) {
    this.socketService.emit(SocketEventsEnum.columnsDelete, {
      boardId,
      columnId,
    });
  }

  updateColumn(boardId: string, columnId: string, fields: { title: string }) {
    this.socketService.emit(SocketEventsEnum.columnsUpdate, {
      boardId,
      columnId,
      fields,
    });
  }
}
