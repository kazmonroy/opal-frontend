import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TaskInterface, TaskUpdateInterface } from '../types/task.interface';
import { BehaviorSubject, Observable } from 'rxjs';
import { TaskInputRequest } from '../types/TaskInputRequest.interface';
import { SocketService } from './socket.service';
import { SocketEventsEnum } from '../types/socketEvents.enum';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  private http = inject(HttpClient);
  private url = environment.apiUrl + '/boards';
  private socketService = inject(SocketService);

  getTasks(boardId: string): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(`${this.url}/${boardId}/tasks`);
  }

  createTask(taskInput: TaskInputRequest) {
    this.socketService.emit(SocketEventsEnum.tasksCreate, taskInput);
  }

  updateTask(boardId: string, taskId: string, fields: TaskUpdateInterface) {
    this.socketService.emit(SocketEventsEnum.tasksUpdate, {
      boardId,
      taskId,
      fields,
    });
  }

  deleteTask(boardId: string, taskId: string) {
    this.socketService.emit(SocketEventsEnum.tasksDelete, {
      boardId,
      taskId,
    });
  }
}
