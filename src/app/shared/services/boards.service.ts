import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BoardInterface } from '../types/board.interface';
import { SocketService } from './socket.service';
import { SocketEventsEnum } from '../types/socketEvents.enum';

@Injectable({
  providedIn: 'root',
})
export class BoardsService {
  private http = inject(HttpClient);
  private socketService = inject(SocketService);
  private url = environment.apiUrl + `/boards`;

  getBoards(): Observable<BoardInterface[]> {
    return this.http.get<BoardInterface[]>(this.url);
  }

  getBoard(boardId: string): Observable<BoardInterface> {
    return this.http.get<BoardInterface>(`${this.url}/${boardId}`);
  }

  createBoard(title: string): Observable<BoardInterface> {
    return this.http.post<BoardInterface>(this.url, { title });
  }

  updateBoard(boardId: string, fields: { title: string }) {
    this.socketService.emit(SocketEventsEnum.boardsUpdate, { boardId, fields });
  }

  deleteBoard(boardId: string) {
    this.socketService.emit(SocketEventsEnum.boardsDelete, { boardId });
  }
}
