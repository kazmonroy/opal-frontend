import { Injectable } from '@angular/core';
import { CurrentUserInterface } from 'src/app/auth/types/currentUser.interface';
import { Socket, io } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  private socket!: Socket;

  setupSocketConnection(currentUser: CurrentUserInterface) {
    this.socket = io(environment.socketUrl, {
      auth: {
        token: currentUser.token,
      },
    });
  }

  disconnect() {
    if (!this.socket) {
      throw new Error('Socket connection is not established');
    }
    this.socket.disconnect();
  }

  emit(eventName: string, message: any) {
    if (!this.socket) {
      throw new Error('Socket connection is not established');
    }

    this.socket.emit(eventName, message);
  }

  listen<T>(eventName: string): Observable<T> {
    if (!this.socket) {
      throw new Error('Socket connection is not established');
    }

    return new Observable((subscriber) => {
      this.socket.on(eventName, (data) => {
        subscriber.next(data);
      });
    });
  }
}
