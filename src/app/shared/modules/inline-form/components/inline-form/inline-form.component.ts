import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  selector: 'app-inline-form',
  templateUrl: './inline-form.component.html',
  styleUrls: ['./inline-form.component.scss'],
})
export class InlineFormComponent {
  private fb = inject(FormBuilder);
  @Input() title: string = '';
  @Input() defaultText: string = 'Not defined';
  @Input() inlineTextPlaceholder: string = '';
  @Input() hasButton: boolean = false;
  @Input() buttonText: string = 'Submit';
  @Input() inputPlaceholder: string = '';
  @Input() inputType: string = 'input';

  @Output() handleSubmit = new EventEmitter<string>();
  @Input() isEditing: boolean = false;

  form = this.fb.group({
    title: [''],
  });

  activateEditing() {
    if (this.title) {
      this.form.patchValue({ title: this.title });
    }
    this.isEditing = true;
  }

  onSubmit() {
    if (this.form.value.title) {
      this.handleSubmit.emit(this.form.value.title);
    }

    this.isEditing = false;
    this.form.reset();
  }
}
