import { CommonModule } from '@angular/common';
import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  inject,
} from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { BehaviorSubject, Observable, combineLatest, map, take } from 'rxjs';
import { TasksService } from 'src/app/shared/services/tasks.service';
import { AutoFocusDirective } from 'src/app/shared/directives/auto-focus.directive';
import { CloseBarDirective } from 'src/app/shared/directives/close-bar.directive';
import { TaskInterface } from 'src/app/shared/types/task.interface';
import { ResultsService } from 'src/app/searchResults/results.service';
import { UserInterface } from 'src/app/shared/types/users.interface';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],

  imports: [CommonModule, RouterModule, AutoFocusDirective, CloseBarDirective],
  standalone: true,
})
export class SearchBarComponent implements OnInit {
  private router = inject(Router);

  private resultsService = inject(ResultsService);
  data$ = new Observable<UserInterface[]>();
  searchQuery$ = new BehaviorSubject<string>('');

  query = '';
  toggleResults = false;

  ngOnInit(): void {
    this.resultsService.getUsers();

    this.data$ = combineLatest([
      this.searchQuery$,
      this.resultsService.users,
    ]).pipe(
      map(([searchQuery, data]) =>
        data.filter((x) => x.name.toLowerCase().includes(searchQuery))
      )
    );
  }

  @HostListener('document:keydown', ['$event']) onKeydownHandler(
    event: KeyboardEvent
  ) {
    if (event.key === 'Escape') {
      this.toggleResults = false;
    }
  }

  searchQuery(queryInput: string) {
    this.searchQuery$.next(queryInput.toLowerCase());
    this.query = queryInput;
    this.query ? (this.toggleResults = true) : (this.toggleResults = false);
  }

  showResults(event: any) {
    event.preventDefault();

    this.data$.pipe(take(1)).subscribe((results) => {
      this.resultsService.updateResults(results);
    });

    this.router.navigateByUrl('/user/results');
  }
}
