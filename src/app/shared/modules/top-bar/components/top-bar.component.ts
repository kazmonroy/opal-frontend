import { Component, HostBinding, HostListener, inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ToggleService } from 'src/app/shared/services/toggle.service';
import { IconsModule } from '../../icons/icons.module';
import { InlineFormComponent } from '../../inline-form/components/inline-form/inline-form.component';
import { CommonModule } from '@angular/common';
import { AutoFocusDirective } from 'src/app/shared/directives/auto-focus.directive';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { CloseBarDirective } from 'src/app/shared/directives/close-bar.directive';
import { ToggleDropdownDirective } from 'src/app/shared/directives/toggle-dropdown.directive';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    IconsModule,
    InlineFormComponent,
    AutoFocusDirective,
    SearchBarComponent,
    ToggleDropdownDirective,
  ],
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent {
  private authService = inject(AuthService);
  private router = inject(Router);
  private toggleService = inject(ToggleService);

  currentUser$ = this.authService.currentUser$;
  query = '';
  isSearchBarOpen = false;

  logOut() {
    this.authService.logOut();
    this.router.navigateByUrl('/');
  }

  toggleSidebar() {
    this.toggleService.handleToggle();
  }
}
