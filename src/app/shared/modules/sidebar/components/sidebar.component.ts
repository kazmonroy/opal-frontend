import { CommonModule } from '@angular/common';
import { Component, OnChanges, OnInit, inject } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Observable } from 'rxjs';
import { BoardsService } from 'src/app/shared/services/boards.service';
import { ToggleService } from 'src/app/shared/services/toggle.service';
import { BoardInterface } from 'src/app/shared/types/board.interface';

@Component({
  standalone: true,
  imports: [CommonModule, RouterModule],
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  host: { class: 'app-sidebar' },
})
export class SidebarComponent {
  private boardsService = inject(BoardsService);

  boards$ = this.boardsService.getBoards();
}
