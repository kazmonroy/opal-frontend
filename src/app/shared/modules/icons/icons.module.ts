import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatherModule } from 'angular-feather';
import {
  Trello,
  LogOut,
  Github,
  Home,
  X,
  Search,
  Gitlab,
  Menu,
  ChevronDown,
} from 'angular-feather/icons';

const icons = {
  Trello,
  LogOut,
  Github,
  Home,
  X,
  Search,
  Gitlab,
  Menu,
  ChevronDown,
};

@NgModule({
  imports: [FeatherModule.pick(icons)],
  exports: [FeatherModule],
})
export class IconsModule {}
