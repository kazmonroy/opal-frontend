export interface TaskInterface {
  id: string;
  title: string;
  description?: string;
  userId: string;
  boardId: string;
  columnId: string;
}

export interface TaskUpdateInterface {
  title?: string;
  description?: string;
  columnId?: string | null;
}
