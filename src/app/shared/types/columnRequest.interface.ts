export interface ColumnInputRequest {
  title: string;
  boardId: string;
}
