export interface TaskInputRequest {
  title: string;
  columnId: string;
  boardId: string;
}
