import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../shared/modules/icons/icons.module';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule, IconsModule],
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {}
