export interface SignUpRequestInterface {
  email: string;
  username: string;
  password: string;
}
