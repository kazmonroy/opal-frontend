import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { HttpErrorResponse } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { SocketService } from 'src/app/shared/services/socket.service';
import { CommonModule } from '@angular/common';
import { AuthService } from '../../services/auth.service';
import { SignUpRequestInterface } from '../../types/signupRequest.interface';

@Component({
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, RouterModule],
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  private fb = inject(FormBuilder);
  private authService = inject(AuthService);
  private router = inject(Router);
  private socketService = inject(SocketService);
  errorMessage: string | null = null;

  form = this.fb.group({
    email: ['', Validators.required],
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  onSubmit() {
    this.authService
      .signUp(this.form!.value as SignUpRequestInterface)
      .subscribe({
        next: (currentUser) => {
          this.authService.setToken(currentUser);
          this.authService.setCurrentUser(currentUser);
          this.socketService.setupSocketConnection(currentUser);
          this.errorMessage = null;
          this.router.navigateByUrl('/');
        },
        error: (err: HttpErrorResponse) => {
          console.log(err.error);
          this.errorMessage = err.error.join(', ');
        },
      });
  }
}
