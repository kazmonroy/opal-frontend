import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { AuthService } from './auth.service';
import { Observable, map, skipWhile, take, tap } from 'rxjs';

export const AuthGuard = ():
  | boolean
  | UrlTree
  | Observable<boolean | UrlTree | null>
  | Promise<boolean | UrlTree> => {
  const authService = inject(AuthService);
  const router = inject(Router);
  return authService.isLoggedIn$.pipe(
    skipWhile((value) => value === null),
    take(1),
    tap((isAuthenticated) => {
      if (!isAuthenticated) {
        router.navigateByUrl('/');
      }
    })
  );
};
