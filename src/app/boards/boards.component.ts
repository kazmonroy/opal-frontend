import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InlineFormComponent } from 'src/app/shared/modules/inline-form/components/inline-form/inline-form.component';
import { BoardsService } from 'src/app/shared/services/boards.service';
import { BoardInterface } from 'src/app/shared/types/board.interface';

@Component({
  standalone: true,
  imports: [CommonModule, RouterModule, InlineFormComponent],
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss'],
})
export class BoardsComponent implements OnInit {
  private boardsService = inject(BoardsService);
  boards: BoardInterface[] = [];

  ngOnInit(): void {
    this.boardsService.getBoards().subscribe((boards) => {
      this.boards = boards;
    });
  }

  createBoard(title: string) {
    this.boardsService.createBoard(title).subscribe((newBoard) => {
      this.boards = [...this.boards, newBoard];
    });
  }
}
